## eof-append
A simple utility for emulating BASH [heredoc](http://en.wikipedia.org/wiki/Here_document) functionality to append lines of content to another file. Use this when you can't actually use heredoc because of file parsing restrictions like when writing a RUN command in a Dockerfile.

## Why?
In [Best practices for writing Dockerfiles](https://docs.docker.com/articles/dockerfile_best-practices/), command splitting across lines using trailing backward slash syntax is encouraged:

```sh
RUN mdkir -p /usr/src/things \
    && curl -SL http://example.com/big.tar.gz \
    | tar -xJC /usr/src/things \
    && make -C /usr/src/things all
```

But many developers are surprised to discover that just because the Dockerfile format supports line splitting in this way it doesn't mean that the format supports other patterns commonly deployed to foster readability and maintainability like the [heredoc](http://en.wikipedia.org/wiki/Here_document) pattern.

Developers rely on the heredoc pattern to output data that must be appended to other files: such as configuration files that may be customized during install. While using [echo](http://wiki.bash-hackers.org/commands/builtin/echo) combined with redirection will work for simple tasks:

```sh
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
```

...this method becomes unwieldy when complexity increases:

```sh
RUN echo '\nserver_name localhost;\nroot /home/brimir/public;\npassenger_enabled on;\n}\n}\ndaemon off;' >> /tmp/nginx.conf
```

The above example certainly isn't very readable and the following attempt to clarify with the use of command splitting doesn't help things much either:

```sh
RUN echo '\n' >> /tmp/nginx.conf; \
  echo 'server_name localhost;\n' >> /tmp/nginx.conf; \
  echo 'root /home/brimir/public;\n' >> /tmp/nginx.conf; \
  echo 'passenger_enabled on; >> /tmp/nginx.conf; \
  echo 'daemon off;' >> /tmp/nginx.conf
```

What the developer really wants to do is use heredoc:

```sh
RUN cat >> echo /tmp/nginx.conf << 'EOF'
server_name localhost;
root /home/brimir/public;
passenger_enabled on;
daemon off;
EOF
```

Unfortunately, the Dockerfile format interpreter doesn't support the use of heredoc syntax. The next option is to create a script that can be called with `RUN` and let the script just take care of these types of changes. But calling external scripts to apply changes circumvents the Dockerfile caching mechanism; even worse, you will need to force an invalidation of the cache in order to get any future changes to appear when again issuing the `docker build` command.

With eof-append, you can get back to writing sane RUN blocks that append data to files. An important benefit is that this also fully supports Dockerfile caching.

### Argument Syntax
With the help of eof-append, the benefits of heredoc can be emulated through an arbitrary number of command line arguments that are fed to the utility. The syntax looks like this:

```sh
RUN /build/bin/eof-append.sh -o /etc/hosts -- \
  "@--" \
  "# internal db servers" \
  "10.0.1.2  db-serv1  db-serv1.pvt.domain.com" \
  "10.0.1.3  db-serv2  db-serv2.pvt.domain.com" \
  "@--"  
```

In the above example, we are simply appending the following lines to the /etc/hosts file (specified by the `-o` option):

* a blank line (indicated by `@--`)
* followed by a comment line
* followed by two configuration lines
* followed by a blank line

The above assumes the eof-append script has been copied to the /build/bin directory on the target.

### Heredoc Syntax
Just for the sake of flexibility and completeness, eof-append also supports heredoc mode where the syntax is supported (that is, not in a Dockerfile). Here's an example:

```sh
bash eof-append.sh -i test/input.conf -o /var/tmp/eof_outfile4.txt -l 2 -t 4 <<EOF
  @--
  # enable some setting in some config file    
  some_setting=true
  @--
EOF
```

### Identifying Blank Lines
A special character sequence `@--` is reserved to identify blank lines. The sequence must appear individually (in between quotes, if passed among arguments). The use of the sequence is necessary only when using eof-append in argument mode; when using eof-append in heredoc mode, the use of this sequence is optional and blank lines will otherwise be preserved as such.


## Installation
To install eof-append, just copy the script to a suitable directory (like /usr/local/bin) as the superuser. Then, execute the following permissions changes:

```sh
>cd /path/to/install/directory
>chown root:root eof-append.sh
>chmod 755 eof-append.sh
>ln -s eof-append.sh eof-append
```

After the execute bit has been set on the eof-append.sh script, you will be able to call the script without having to invoke a shell first and then pass the script as an argument.

**NOTE: fake-swapon requires BASH 4.2 or greater.**

### Installation for Dockerfile
To use eof-append with Dockerfile, you will need to copy the script into an executable place during the build process.

```sh
RUN mkdir -p /build/bin /build/tmp
WORKDIR /build
RUN curl -o tmp/eof-append.bz2 https://bitbucket.org/tapatip/eof-append/get/v1.0.4.tar.bz2 \
  && arc="tmp/eof-append.bz2"; file="eof-append.sh";  \
    file="$(tar tjf ${arc} | grep ${file})"; \
    dir="$(dirname ${file})" \
  && tar xjf $arc -C tmp $file && cp tmp/$file bin
```

The script is then called with its full path.

## Usage
There are two modes of usage:

* argument mode
* heredoc mode

If no arguments follow the specification of options, then eof-append expects its arguments on stdin, specifically using heredoc syntax. __You must specify the end of options option `--` when providing arguments after options.__

For help, just invoke eof-append with the help `(-h | --help)` option:

```sh
>eof-append --help
```

Options include:

```
   -i, --in-file                Input file
   -o, --out-file               Output file
   -l, --trim-lead              Number of leading chars to strip
   -t, --trim-tail              Number of trailing chars to strip
   -d, --debug                  Turn debugging on (increases verbosity)
   -f, --force                  Execute without user prompt
   -h, --help                   Show this message
   -v, --version                Output version of this script
   --                           End option arguments
```

**NOTE: eof-append only works with plain text files.**

### Input File
If an input file is specified, content from the input file will be output before lines added as argument input or as heredoc input.

### Output File
Content from the input file (if specified) followed by lines added as argument input or as heredoc input will appended to the specified output file. The file will be created if it doesn't exist. If no output file is specified, then output will be sent to stdout.

### Trim Lead
The maximum number of space characters to trim from the beginning of each line. This is handy when using heredoc mode where lines may be space-indented for clarity. The default value is 0.

### Trim Tail
The maximum number of space characters to trim from the end of each line. Provided for consistency. The default value is 0.

## Limitations
The eof-append utility is meant to emulate the key features of heredoc support. It is by no means a replacement for genuine heredoc support.

**NOTE: eof-append only works with plain text files.**

## Compatibility
eof-append has been developed and tested on OSX and Linux. Supported Linux distributions currently include:

* [CoreOS](https://coreos.com/)
* [CentOS](http://www.centos.org/)

eof-append requires BASH 4.0 or higher.

## License
eof-append is licensed under the MIT open source license.
