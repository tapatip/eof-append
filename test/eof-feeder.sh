#!/usr/bin/env bash
#
# STEmacsModelines:
# -*- Shell-Unix-Generic -*-
#
# Create and enable a swap file on Linux.
#

echo "With command line args only..."
bash eof-append.sh -d -o /var/tmp/eof_outfile1.txt -t 2 '\
  "@--" \
  "# Enable sudoers secure_path for rvmsudo" \
  "export rvmsudo_secure_path=1" \
  "@--"'

echo "With command line args, input file, output file..."
bash eof-append.sh -d -i test/input.conf -o /var/tmp/eof_outfile2.txt -t 2 '\
  "@--" \
  "# Enable sudoers secure_path for rvmsudo" \
  "export rvmsudo_secure_path=1" \
  "@--"'

echo "With EOF heredoc syntax, output file..."
bash eof-append.sh -d -o /var/tmp/blk/eof_outfile3.txt -l 2 -t 4 <<EOF

  # enable some setting in some config file    
  some_setting=true
  @--
EOF

echo "With EOF heredoc syntax, input and output file..."
# bash eof-append.sh -d -i test/input.conf -o /var/tmp/eof_outfile.txt -l 2 -t 4 <<EOF
bash eof-append.sh -d -i test/input.conf -o /var/tmp/eof_outfile4.txt -l 2 -t 4 <<EOF

  # enable some setting in some config file    
  some_setting=true
  @--
EOF

# echo "With no args or heredoc..."
# bash eof-append.sh -d -o /path/to/my/outfile.txt -l 2 -t 1

