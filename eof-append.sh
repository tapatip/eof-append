#!/usr/bin/env bash
#
# STEmacsModelines:
# -*- Shell-Unix-Generic -*-
#
# Create and enable a swap file on Linux.
#

# Copyright (c) 2014 Mark Eissler, mark@mixtur.com

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PATH=/usr/local/bin

PATH_BNAME="/usr/bin/basename"
PATH_GETOPT="/usr/bin/getopt"
PATH_CAT="/bin/cat"
PATH_SED="/usr/bin/sed"
PATH_RM="/bin/rm"
PATH_TOUCH="/usr/bin/touch"
PATH_BASENAME="/usr/bin/basename"
PATH_DIRNAME="/usr/bin/dirname"

# path fixups for some systems
[ ! -f "${PATH_SED}" ] && PATH_SED="/bin/sed"
[ ! -f "${PATH_TOUCH}" ] && PATH_TOUCH="/bin/touch"
[ ! -f "${PATH_BNAME}" ] && PATH_BNAME="/bin/basename"

###### NO SERVICABLE PARTS BELOW ######
VERSION=1.0.5
PROGNAME=$(${PATH_BNAME} $0)

# reset internal vars (do not touch these here)
DEBUG=0
GETOPT_OLD=0
TRIMLEAD=0
TRIMTAIL=0
EMPTYSTR=""
PATH_INFILE=""
PATH_OUTFILE=""

# min bash required
VERS_BASH_MAJOR=4
VERS_BASH_MINOR=2
VERS_BASH_PATCH=0



#
# FUNCTIONS
#

function usage {
  if [ ${GETOPT_OLD} -eq 1 ]; then
    usage_old
  else
    usage_new
  fi
}

function usage_new {
${PATH_CAT} << EOF
usage: ${PROGNAME} [options] args

Append content to a file by passing each line of content separately as a quoted
argument or inside the body of a heredoc block.

OPTIONS:
   -i, --in-file                Input file
   -o, --out-file               Output file
   -l, --trim-lead              Number of leading chars to strip
   -t, --trim-tail              Number of trailing chars to strip
   -d, --debug                  Turn debugging on (increases verbosity)
   -f, --force                  Execute without user prompt
   -h, --help                   Show this message
   -v, --version                Output version of this script
   --                           End option arguments

Command line arguments
----------------------
Each line to append can be passed as a separate command line argument. These
arguments must be passed following all option specifications and each argument
must be individually quoted.

You must signal the end of options by specifying the -- option before the first
line of content argument.

Heredoc arguments
-----------------
Arguments can also be passed with heredoc syntax. Following options, simply
append your heredoc following the << symbol and a limit string. The block is
closed with the specified limit string appearing on a separate line.

Input and Output files
----------------------
If no input file is specified (with the --in-file option) output will only
consist of concatenated args or heredoc lines. Output will be to stdout if no
output file is specified (with the --out-file option).

EOF
}

# support for old getopt (non-enhanced, only supports short param names)
#
function usage_old {
${PATH_CAT} << EOF
usage: ${PROGNAME} [options] args

Append content to a file by passing each line of content separately as a quoted
argument or inside the body of a heredoc input stream.

OPTIONS:
   -i                           Input file
   -o                           Output file
   -l                           Number of leading chars to strip
   -t                           Number of trailing chars to strip
   -d                           Turn debugging on (increases verbosity)
   -f                           Execute without user prompt
   -h                           Show this message
   -v                           Output version of this script
   --                           End option arguments

Command line arguments
----------------------
Each line to append can be passed as a separate command line argument. These
arguments must be passed following all option specifications and each argument
must be individually quoted.

You must signal the end of options by specifying the -- option before the first
line of content argument.

Heredoc arguments
-----------------
Arguments can also be passed with heredoc syntax. Following options, simply
append your heredoc following the << symbol and a limit string. The block is
closed with the specified limit string appearing on a separate line.

Input and Output files
----------------------
If no input file is specified (with the -i option) output will only consist of
concatenated args or heredoc lines. Output will be to stdout if no output file
is specified (with the -o option).

EOF
}

function promptHelp {
${PATH_CAT} << EOF
For help, run "${PROGNAME}" with the -h flag or without any options.

EOF
}

function version {
  echo ${PROGNAME} ${VERSION};
}

# promptConfirm()
#
# Confirm a user action. Input case insensitive.
#
# Returns "yes" or "no" (default).
#
function promptConfirm() {
  read -p "$1 ([y]es or [N]o): "
  case $(echo $REPLY | ${PATH_TR} '[A-Z]' '[a-z]') in
    y|yes) echo "yes" ;;
    *)     echo "no" ;;
  esac
}

# isNumber()
# /*!
# @abstract Check if a given string is a number
# @discussion
# Parses a string to test whether its components are digits including decimals.
#   The string can be preceded by a negative sign to indicate a negative value.
# @param numberStr String to check.
# @return Returns 1 on success (status 0), 0 on failure (status 1).
# */
isNumber() {
  if [[ ${1} =~ ^-?[0-9]+([.][0-9]+)*$ ]]; then
    # match
    echo 1; return 0
  else
    # no match
    echo 0; return 1
  fi
}

# isPathWriteable()
#
# Checks if a given path (file or directory) is writeable by the current user.
#
isPathWriteable() {
  if [ -z "${1}" ]; then
    echo 0; return 1
  fi

  # path is a directory...
  if [[ -d "${1}" ]]; then
    local path="${1%/}/.test"
    local resp rslt
    resp=$({ ${PATH_TOUCH} "${path}"; } 2>&1)
    rslt=$?
    if [[ ${rslt} -ne 0 ]]; then
      # not writeable directory
      echo 0; return 1
    else
      # writeable directory
      ${PATH_RM} "${path}"
      echo 1; return 0
    fi
  fi

  # path is a file...
  if [ -w "${1}" ]; then
    # writeable file
    echo 1; return 0
  else
    # not writeable file
    echo 0; return 1
  fi

  # and if we fall through...
  echo 0; return 128
}

# compareVersions()
#
# Compares two SemVer version strings and returns:
#   0 - versions are equal
#   1 - version A is greater than version B
#   2 - version A is less than version B
#
compareVersions() {
    if [[ $1 == $2 ]]
    then
        return 0
    fi

    local IFS=.
    local i ver1=($1) ver2=($2)

    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done

    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done

    return 0
}

# check for minimum bash
bashVersion="${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}.${BASH_VERSINFO[2]}"
needVersion="${VERS_BASH_MAJOR}.${VERS_BASH_MINOR}.${VERS_BASH_PATCH}"
if [[ $(compareVersions ${bashVersion} ${needVersion}; echo $?) -eq 2 ]]; then
  echo -n "${PROGNAME} requires at least BASH ${VERS_BASH_MAJOR}.${VERS_BASH_MINOR}.${VERS_BASH_PATCH}!"
  echo " (I seem to be running in BASH ${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}.${BASH_VERSINFO[2]})"
  echo
  exit 100
fi

# parse cli parameters
#
# Our options:
#   --out-file, o
#   --trim-lead, l
#   --trim-tail, t
#   --debug, d
#   --force, f
#   --help, h
#   --version, v
#
params=()
${PATH_GETOPT} -T > /dev/null
if [ $? -eq 4 ]; then
  # GNU enhanced getopt is available
  PROGNAME=$(${PATH_BNAME} $0)
  cliarg="$(${PATH_GETOPT} --name "$PROGNAME" --long in-file:,out-file:,trim-lead:,trim-tail:,force,help,version,debug --options i:o:l:t:fhvd)"
  params+=( ${cliargs} )
  params+=( "$@" )
else
  # Original getopt is available
  GETOPT_OLD=1
  PROGNAME=$(${PATH_BNAME} $0)
  params=( "$(${PATH_GETOPT} i:o:l:t:fhvd)" )
  params+=( "$@" )
fi

# check for invalid params passed; bail out if error is set.
if [ $? -ne 0 ]
then
  usage; exit 1;
fi

set -- "${params[@]}"
# fix for old getopt inserting an extra "--" in the params
if [ ${GETOPT_OLD} -ne 0 ]; then
  shift
fi
unset params

while [ $# -gt 0 ]; do
  case "$1" in
    -i | --in-file)         cli_INFILE="$2"; shift;;
    -o | --out-file)        cli_OUTFILE="$2"; shift;;
    -l | --trim-lead)       cli_TRIMLEAD="$2"; shift;;
    -t | --trim-tail)       cli_TRIMTAIL="$2"; shift;;
    -d | --debug)           cli_DEBUG=1; DEBUG=${cli_DEBUG};;
    -f | --force)
      cli_FORCEEXEC=1; FORCEEXEC=${cli_FORCEEXEC};
      if [[ ${DEBUG} -ne 0 ]]; then
        echo "-f option not implemented";
      fi
      ;;
    -v | --version)         version; exit;;
    -h | --help)            usage; exit;;
    --)                     cli_ENDARGS=1; shift; break;;
  esac
  shift
done

argavail=$#
argparse=0
arglines_array=()

# parse args or parse heredoc lines (read stdin if no args)
if [[ ${DEBUG} -ne 0 ]]; then
  echo "DEBUG: cliargs: ${argavail}"
fi

if [[ ${argavail} -gt 0 ]]; then
  # args available; parse them
  while [[ ${argavail} -gt 0 ]]; do
    (( argparse++ ))
    if [[ ${DEBUG} -ne 0 ]]; then
      printf "DEBUG: %02d: -|%s\n" "${argparse}" "${1}"
    fi
    arglines_array+=("${1}")
    shift
    argavail=$#
  done
else
  if [[ -z "${cli_ENDARGS+xxx}" ]] || \
    [[ "${cli_ENDARGS+xxx}" == "xxx" && ${cli_ENDARGS} -eq 0 ]]; then
    # no args, must be stdin; read heredoc lines
    while IFS= read -r line; do
      (( argparse++ ))
      if [[ ${DEBUG} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${line}"
      fi
      arglines_array+=("${line}")
    done
  fi
fi

if [[ ${argparse} -eq 0 ]]; then
  echo
  echo "ABORTING. No arguments found."
  echo
  usage
  exit 1
fi

# Rangle our vars
#
if [ -n "${cli_TRIMLEAD}" ]; then
  if [[ $(isNumber "${cli_TRIMLEAD}") -eq 1 && ${cli_TRIMLEAD} -gt 0 ]]; then
    TRIMLEAD=${cli_TRIMLEAD};
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: trimlead: ${cli_TRIMLEAD} (ok)"
    fi
  else
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: trimlead: ${cli_TRIMLEAD} (invalid)"
    fi
  fi
fi

if [ -n "${cli_TRIMTAIL}" ]; then
  if [[ $(isNumber "${cli_TRIMTAIL}") -eq 1 && ${cli_TRIMTAIL} -gt 0 ]]; then
    TRIMTAIL=${cli_TRIMTAIL};
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: trimtail: ${cli_TRIMTAIL} (ok)"
    fi
  else
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: trimtail: ${cli_TRIMTAIL} (invalid)"
    fi
  fi
fi

if [ -n "${cli_INFILE}" ]; then
  PATH_INFILE="${cli_INFILE}";

  if [[ -f "${PATH_INFILE}" ]] && [[ ! -r "${PATH_INFILE}" ]]; then
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access input file: ${PATH_INFILE}"
      echo "DEBUG: Must be a read permissions error."
    fi
    exit 1
  elif [[ ! -f "${PATH_INFILE}" ]]; then
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access input file: ${PATH_INFILE}"
      echo "DEBUG: The target file appears to be missing."
    fi
    exit 1
  fi
fi

if [ -n "${cli_OUTFILE}" ]; then
  PATH_OUTFILE="${cli_OUTFILE}";

  if [[ -f "${PATH_OUTFILE}" ]] && [[ ! -w "${PATH_OUTFILE}" ]]; then
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access output file: ${PATH_OUTFILE}"
      echo "DEBUG: Must be a write permissions error."
    fi
    exit 1
  elif [[ ! -f "${PATH_OUTFILE}" ]]; then
    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: Unable to access output file: ${PATH_OUTFILE}"
      echo "DEBUG: The target file appears to be missing."
    fi

    # create output file
    _outfiledir=$(${PATH_DIRNAME} "${PATH_OUTFILE}")
    if [[ -d "${_outfiledir}" ]] && [[ $(isPathWriteable "${_outfiledir}") -ne 1 ]]; then
      if [[ ${DEBUG} -ne 0 ]]; then
        echo "DEBUG: Unable to access output file directory: ${_outfiledir}"
        echo "DEBUG: Must be a write permissions error."
      fi
      exit 1
    elif [[ ! -d "${_outfiledir}" ]]; then
      if [[ ${DEBUG} -ne 0 ]]; then
        echo "DEBUG: Unable to access output file directory: ${_outfiledir}"
        echo "DEBUG: The target directory appears to be missing."
      fi
      exit 1
    fi

    if [[ ${DEBUG} -ne 0 ]]; then
      echo "DEBUG: Creating output file at path: ${PATH_OUTFILE}"
    fi

    _resp=$({ $PATH_TOUCH "${PATH_OUTFILE}"; } 2>&1 )
    _rslt=$?
    if [[ ${_rslt} -ne 0 ]]; then
      echo "ABORTING. Unable to create output file: ${PATH_OUTFILE}"
      echo "Not sure what the problem is."
      exit 1
    fi
    unset _rslt
    unset _resp
    unset _outfiledir
  fi
fi

# Convert blank line indicators, strip leading and trailing
_line=""
for((i=0,idx=1; i<${argparse}; i++,idx++)); do
  _line="${arglines_array[$i]}"
  #
  # remove leading chars
  #
  if [[ ${DEBUG} -ne 0 ]]; then
    printf "DEBUG: ===\n"
    printf "DEBUG: %02d (munge 01 beg): -|%s|-\n" "${idx}" "${_line}"
  fi
  _line=$(echo "${_line}" | ${PATH_SED} "s/^[ ]\{0,${TRIMLEAD}\}//" )
  if [[ ${DEBUG} -ne 0 ]]; then
    printf "DEBUG: %02d (munge 01 end): -|%s|-\n" "${idx}" "${_line}"
  fi

  #
  # remove trailing chars
  #
  if [[ ${DEBUG} -ne 0 ]]; then
    printf "DEBUG: %02d (munge 02 beg): -|%s|-\n" "${idx}" "${_line}"
  fi
  _line=$(echo "${_line}" | ${PATH_SED} "s/[ ]\{0,${TRIMTAIL}\}$//" )
  if [[ ${DEBUG} -ne 0 ]]; then
    printf "DEBUG: %02d (munge 02 end): -|%s|-\n" "${idx}" "${_line}"
  fi

  #
  # transform blank line indicator
  #
  if [[ ${DEBUG} -ne 0 ]]; then
    printf "DEBUG: %02d (munge 03 beg): -|%s|-\n" "${idx}" "${_line}"
  fi
  _line=$(echo "${_line}" | ${PATH_SED} -E "s/^@--$//" )
  if [[ ${DEBUG} -ne 0 ]]; then
    printf "DEBUG: %02d (munge 03 end): -|%s|-\n" "${idx}" "${_line}"
  fi

  # write back
  arglines_array[$i]="${_line}"
done
unset _line

if [[ -n "${PATH_OUTFILE}" ]]; then
  _resp=""
  #
  # append to outfile
  #
  if [[ -n "${PATH_INFILE}" ]]; then
    _resp=$({ ${PATH_CAT} "${PATH_INFILE}" >> "${PATH_OUTFILE}"; } 2>&1)
  fi

  for((i=0; i<${argparse}; i++)); do
    _resp=$({ echo "${arglines_array[$i]}" >> "${PATH_OUTFILE}"; } 2>&1)
  done
  unset _resp
else
  _resp=""
  #
  # echo to stdout
  #
  if [[ -n "${PATH_INFILE}" ]]; then
    # capture stderr, let stdout through
    { _resp=$(${PATH_CAT} ${PATH_INFILE} 2>&1 1>&3-) ;} 3>&1
  fi

  for((i=0; i<${argparse}; i++)); do
    # capture stderr, let stdout through
    { _resp=$(echo "${arglines_array[$i]}" 2>&1 1>&3-) ;} 3>&1
  done
  unset _resp
fi
